//
//  ArticleTableViewCell.swift
//  MostPopular
//
//  Created by Cadabra on 8/5/19.
//  Copyright © 2019 BDia. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bylineLabel: UILabel!
    
    
    func configuration(title: String, byline: String) {
        titleLabel.text = title
        bylineLabel.text = byline
    }
    
}
