//
//  DetailViewController.swift
//  MostPopular
//
//  Created by Cadabra on 8/4/19.
//  Copyright © 2019 BDia. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var abstractLabel: UILabel!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var copyrightLabel: UILabel!
    @IBOutlet weak var bylineLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var addToFavoriteButton: UIButton!
    
    var model = Model()
    var isFavorite: Bool = false {
        didSet {
            if isFavorite {
                addToFavoriteButton.setImage(#imageLiteral(resourceName: "bookmarkFilled"), for: .normal)
            } else {
                addToFavoriteButton.setImage(#imageLiteral(resourceName: "bookmark"), for: .normal)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkIsFavorite()
    }
    
    // MARK: - Helper Methods
    
    func checkIsFavorite() {
        isFavorite = model.isFavorite()
    }
    
    func updateUi() {
        titleLabel.text = model.title
        abstractLabel.text = model.abstract
        bylineLabel.text = model.byline
        if let date = model.publishedDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM dd, yyyy"
            dateLabel.text = dateFormatter.string(from: date)
        }
        if let caption = model.meida?.caption {
            captionLabel.text = caption
        } else {
            captionLabel.removeFromSuperview()
        }
        if let copyright = model.meida?.copyright {
            copyrightLabel.text = copyright
        } else {
            copyrightLabel.removeFromSuperview()
        }
        if let metadata = model.meida?.mediaMetadata {
            var urlStr: String?
            if let imageModel = metadata.filter ({ $0.format == "mediumThreeByTwo440"}).first {
                urlStr = imageModel.url
            } else if let imageModel = metadata.filter ({ $0.format == "mediumThreeByTwo210"}).first {
                urlStr = imageModel.url
            } else if let imageModel = metadata.filter ({ $0.format == "Standard Thumbnail"}).first {
                urlStr = imageModel.url
            }
            
            if let image = model.image {
                self.imageView.image = image
            } else if let urlStr = urlStr, let url = URL(string: urlStr) {
                DispatchQueue.global().async {
                    if let data = try? Data(contentsOf: url) {
                        DispatchQueue.main.async {
                            self.imageView.image = UIImage(data: data)
                            self.model.image = UIImage(data: data)
                        }
                    }
                }
            }
            view.layoutIfNeeded()
        }
    }
    
    // MARK: - Actions
    
    @IBAction func addToFavoritesAction(_ sender: Any) {
        isFavorite = !isFavorite
        isFavorite ? model.saveToCoreData() : model.removeFromCoreData()
    }
}
