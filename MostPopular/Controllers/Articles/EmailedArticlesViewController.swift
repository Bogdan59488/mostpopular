//
//  EmailedArticlesViewController.swift
//  MostPopular
//
//  Created by Cadabra on 8/3/19.
//  Copyright © 2019 BDia. All rights reserved.
//

import UIKit

class EmailedArticlesViewController: ArticlesViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Emailed"
        articlesType = .emailed
        getArticles()
    }
}
