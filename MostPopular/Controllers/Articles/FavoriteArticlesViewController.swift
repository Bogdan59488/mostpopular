//
//  FavoriteArticlesViewController.swift
//  MostPopular
//
//  Created by Cadabra on 8/3/19.
//  Copyright © 2019 BDia. All rights reserved.
//

import UIKit

class FavoriteArticlesViewController: ArticlesViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Favorite"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getArticles()
    }
    
    // MARK: - Helper Methods
    
    override func getArticles() {
        self.articlesModels = Model.allFavorite()
        tableView.reloadData()
    }

}
