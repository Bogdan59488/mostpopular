//
//  ArticlesViewController.swift
//  MostPopular
//
//  Created by Cadabra on 8/3/19.
//  Copyright © 2019 BDia. All rights reserved.
//

import UIKit

class ArticlesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var articlesModels = [Model]()
    var articlesType: ArticleType = .emailed
    var period: Int = 30
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurationTableView()
    }
    
    // MARK: - Helper Methods
    
    func configurationTableView() {
        tableView.register(UINib(nibName: "ArticleTableViewCell", bundle: nil), forCellReuseIdentifier: "ArticleCell")
        tableView.estimatedRowHeight = 66
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
    }
    
    func getArticles() {
        NetworkManager().getArticles(articleType: articlesType, period: period) { (results, status) in
            if let results = results as? [[String: Any]] {
                self.articlesModels = [Model]()
                for result in results {
                    let model = Model.fromJson(type: self.articlesType, dict: result)
                    self.articlesModels.append(model)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Actions
    
    @IBAction func refreshButtonPressed(_ sender: Any) {
        getArticles()
    }
}


// MARK: - Table View: Delegate & Data Source

extension ArticlesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articlesModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "ArticleCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? ArticleTableViewCell else { fatalError("Wrong cell type dequeued") }
        let model = articlesModels[indexPath.row]
        let title = model.title ?? ""
        let byline = model.byline ?? ""
        cell.configuration(title: title, byline: byline)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "Detail") as? DetailViewController else { return }
        vc.model = articlesModels[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
