//
//  SharedArticlesViewController.swift
//  MostPopular
//
//  Created by Cadabra on 8/3/19.
//  Copyright © 2019 BDia. All rights reserved.
//

import UIKit

class SharedArticlesViewController: ArticlesViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Shared"
        articlesType = .shared
        getArticles()
    }
}
