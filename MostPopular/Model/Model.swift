//
//  Model.swift
//  MostPopular
//
//  Created by Cadabra on 8/3/19.
//  Copyright © 2019 BDia. All rights reserved.
//

import UIKit
import Foundation

import CoreData

enum ArticleType {
    case emailed
    case shared
    case viewed
}

class Media {
    var caption: String?
    var copyright: String?
    var mediaMetadata: [ImageModel]?
}

class ImageModel {
    var url: String?
    var format: String?
    var height: Float?
    var width: Float?
}

class Model {
    var image: UIImage?
    var articleType: ArticleType = .emailed
    var url: String?
    var column: String?
    var section: String?
    var byline: String?
    var type: String?
    var title: String?
    var abstract: String?
    var publishedDate: Date?
    var source: String?
    var id: Int64?
    var assetId: Int64?
    var meida: Media?
    
    static func fromJson(type: ArticleType, dict: [String: Any]) -> Model {
        let model = Model()
        model.articleType = type
        if let id = dict["id"] as? Int64 {
            model.id = id
        }
        if let asset_id = dict["asset_id"] as? Int64 {
            model.assetId = asset_id
        }
        if let url = dict["url"] as? String {
            model.url = url
        }
        if let column = dict["column"] as? String {
            model.column = column
        }
        if let section = dict["section"] as? String {
            model.section = section
        }
        if let byline = dict["byline"] as? String {
            model.byline = byline
        }
        if let type = dict["type"] as? String {
            model.type = type
        }
        if let title = dict["title"] as? String {
            model.title = title
        }
        if let abstract = dict["abstract"] as? String {
            model.abstract = abstract
        }
        if let publishedDate = dict["published_date"] as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: publishedDate)
            model.publishedDate = date
        }
        if let source = dict["source"] as? String {
            model.source = source
        }
        if let mediaArr = dict["media"] as? [[String: Any]] {
            model.meida = parseMedia(mediaArr)
        }
        
        return model
    }
    
    private static func parseMedia(_ mediaArr: [[String: Any]]) -> Media? {
        guard let media = mediaArr.first else { return nil }
        let mediaModel = Media()
        
        if let caption = media["caption"] as? String {
            mediaModel.caption = caption
        }
        if let copyright = media["copyright"] as? String {
            mediaModel.copyright = copyright
        }
        if let metadata = media["media-metadata"] as? [[String: Any]] {
            var imageModels = [ImageModel]()
            for imageParams in metadata {
                let imageModel = ImageModel()
                if let url = imageParams["url"] as? String {
                    imageModel.url = url
                }
                if let format = imageParams["format"] as? String {
                    imageModel.format = format
                }
                if let height = imageParams["height"] as? Float {
                    imageModel.height = height
                }
                if let width = imageParams["width"] as? Float {
                    imageModel.width = width
                }
                imageModels.append(imageModel)
            }
            mediaModel.mediaMetadata = imageModels
        }
        return mediaModel
    }
    
    func saveToCoreData() {
        let model = self
        let article = ArticleData(context: CoreDataManager.shared.persistentContainer.viewContext)
        let media = MediaData(context: CoreDataManager.shared.persistentContainer.viewContext)
        
        if let metadata = model.meida?.mediaMetadata {
            for imageModel in metadata {
                let image = ImageData(context: CoreDataManager.shared.persistentContainer.viewContext)
                image.url = imageModel.url
                image.format = imageModel.format
                image.height = imageModel.height ?? 0
                image.width = imageModel.width ?? 0
                if let img = model.image {
                    let data = img.jpegData(compressionQuality: 1) as Data?
                    image.imageData = data
                }
                media.addToMediaMetadata(image)
            }
        }
        media.caption = model.meida?.caption
        media.copyright = model.meida?.copyright
        article.media = media
        article.url = model.url
        article.column = model.column
        article.byline = model.byline
        article.title = model.title
        article.abstract = model.abstract
        article.publishedDate = model.publishedDate
        article.source = model.source
        article.id = model.id ?? -1
        
        CoreDataManager.shared.saveContext()
    }
    
    func removeFromCoreData() {
        let model = self
        let fetchRequest: NSFetchRequest<ArticleData> = ArticleData.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %@", String(model.id!))
        let articles = try! CoreDataManager.shared.persistentContainer.viewContext.fetch(fetchRequest)
        
        guard let article = articles.first else { return }
        let shared = CoreDataManager.shared.persistentContainer.viewContext
        shared.delete(article)
        do {
            try shared.save()
        } catch let error as NSError {
            print("Error While Deleting Note: \(error.userInfo)")
        }
    }
    
    func isFavorite() -> Bool {
        let model = self
        let fetchRequest: NSFetchRequest<ArticleData> = ArticleData.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %@", String(model.id!))
        fetchRequest.returnsObjectsAsFaults = true
        let articlesArr = try! CoreDataManager.shared.persistentContainer.viewContext.fetch(fetchRequest)
        return articlesArr.count > 0
    }
    
    static func allFavorite() -> [Model] {
        var models = [Model]()
        let articlesArr = try! CoreDataManager.shared.persistentContainer.viewContext.fetch(ArticleData.fetchRequest())
        guard let articles = articlesArr as? [ArticleData] else { return models }
        
        for article in articles {
            let model = Model()
            model.url = article.url
            model.column = article.column
            model.byline = article.byline
            model.title = article.title
            model.abstract = article.abstract
            model.publishedDate = article.publishedDate
            model.source = article.source
            model.id = article.id
            
            let media = Media()
            media.caption = article.media?.caption
            media.copyright = article.media?.copyright
            
            var imageModels = [ImageModel]()
            if let metadata = article.media?.mediaMetadata as? Set<ImageData> {
                for image in metadata {
                    let imageModel = ImageModel()
                    imageModel.url = image.url
                    imageModel.format = image.format
                    imageModel.height = image.height
                    imageModel.width = image.width
                    if let imageData = image.imageData {
                        model.image = UIImage(data: imageData as Data)
                    }
                    imageModels.append(imageModel)
                }
            }
            media.mediaMetadata = imageModels
            model.meida = media
            models.append(model)
        }
        return models
    }
}
