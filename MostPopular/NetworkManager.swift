//
//  NetworkManager.swift
//  MostPopular
//
//  Created by Bdia on 8/2/19.
//  Copyright © 2019 BDia. All rights reserved.
//

import Foundation
import Alamofire

let baseUrl = "https://api.nytimes.com/svc/mostpopular/v2/"
func appendingBaseUrl(_ url: String) -> String {
    let baseurl: String = "\(baseUrl)\(url)"
    return baseurl
}
typealias Completion = (_ result: Any?, _ status: Bool) -> ()

class NetworkManager {
    private let apiKey = "qXITc2swVU0XUlZ9dQ3y7zGiivDIQHGr"
    
    // MARK: - Requests
    
    func getArticles(articleType: ArticleType, period: Int ,completion: @escaping Completion) {
        let params: Parameters = ["api-key": apiKey]
        var url = ""
        
        switch articleType {
        case .emailed: url = appendingBaseUrl("emailed/\(period)")
        case .shared: url = appendingBaseUrl("emailed/\(period)")
        case .viewed: url = appendingBaseUrl("emailed/\(period)")
        }
        self.request(urlRequest: url, method: .get, params: params) { (response, status) in
            if let response = response as? [String: Any],
                let results = response["results"] as? [[String: Any]] {
                completion(results, status)
            } else {
                completion(response, status)
            }
        }
    }
    
    func request(urlRequest: String, method: HTTPMethod, params: Parameters, encoding: ParameterEncoding = URLEncoding.default, completion: @escaping Completion) {
        let queue = DispatchQueue(label: "com.MostPopular-Test-queue",
                                  qos: .userInitiated,
                                  attributes:.concurrent)
        Alamofire.request(urlRequest, method: method, parameters: params, encoding: encoding).responseJSON(queue: queue,
                                                                                                           options: .allowFragments) { (response: DataResponse<Any>) in
                                                                                                            self.process(response, completion: completion)
        }
    }
    
    private func process(_ response: DataResponse<Any>, completion: @escaping Completion) {
        print("Success: \(response.result.isSuccess)")
        print("Response String: \(String(describing: response.result.value))")
        
        switch(response.result) {
        case .success(_):
            guard let statusCode = response.response?.statusCode else {
                // No response for some reason
                DispatchQueue.main.async {
                    completion(nil, false)
                }
                return
            }
            
            switch statusCode {
            case 403: break
            case 500: break
            default:
                guard let dict = response.result.value as? [String: Any] else {
                    DispatchQueue.main.async {
                        let returnStatus = statusCode >= 200 && statusCode < 300
                        completion(nil, returnStatus)
                    }
                    return
                }
                var returnStatus = true
                var returnValue:Any = dict
                
                if let isError = dict["error"] as? Bool, true == isError {
                    returnStatus = false
                    if let result = dict["result"] as? [String: Any],
                        let message = result["message"] as? String {
                        returnValue = message
                    }
                }
                DispatchQueue.main.async {
                    completion(returnValue, returnStatus)
                }
            }
        case .failure(_):
            if let error = response.result.error as? AFError {
                print(">>>>>>>>> responce error (\(String(describing: response.response?.statusCode))) \(error.localizedDescription)")
            }
            DispatchQueue.main.async {
                completion(nil, false)
            }
        }
    }
}
